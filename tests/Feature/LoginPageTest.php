<?php

namespace Tests\Feature;

use Tests\TestCase;

class LoginPageTest extends TestCase
{
    /**
     * @test
     */
    public function userCanSee()
    {
        $response = $this->get('/login');
        
        $response->assertStatus(200);
    }
    
    /** @test */
    public function userRedirectedIfNotLoggedIn()
    {
        $this->get('/home')
            ->assertRedirect('/login');
    }
}
