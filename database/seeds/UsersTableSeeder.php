<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash as pHash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name', 'admin')->first();
        $authorRole = Role::where('name', 'author')->first();
        $userRole = Role::where('name', 'user')->first();
        
        $admin = User::create([
            'name' => 'Admin User',
            'email' => 'admin@nomail.com',
            'password' => pHash::make('adminadmin')
        ]);
        $author = User::create([
            'name' => 'Author User',
            'email' => 'author@nomail.com',
            'password' => pHash::make('authorauthor')
        ]);
        $user = User::create([
            'name' => 'Generic User',
            'email' => 'user@nomail.com',
            'password' => pHash::make('useruser')
        ]);
        
        $admin->roles()->attach($adminRole);
        $author->roles()->attach($authorRole);
        $user->roles()->attach($userRole);
    }
}
