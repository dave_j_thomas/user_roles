<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Exception;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UsersController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();
        
        return view('admin.users.index', compact('users'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return Response
     */
    public function edit(User $user)
    {
        if (Gate::denies('edit-users')) {
            return redirect('admin/users');
        }
        
        $roles = Role::all();
        
        return view('admin.users.edit', compact('user', 'roles'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  User  $user
     * @return Response
     */
    public function update(Request $request, User $user)
    {
        $user->roles()->sync($request->roles);
        $user->name = $request->name;
        if ($user->save()) {
            $request->session()->flash('success', 'User Update!');
        } else {
            $request->session()->flash('error', 'User Update failed, please try again.');
            
        }
        
        
        return redirect('admin/users');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return Response
     * @throws Exception
     */
    public function destroy(User $user)
    {
        if (Gate::denies('delete-users')) {
            return redirect('admin/users');
        }
        
        $user->roles()->detach();
        $user->delete();
        
        return redirect('admin/users');
    }
}
